# PoC - Libguests x Yara

Main goal of this *Proof of Concept* is to test if we can integrate 
[Guestfs](http://libguestfs.org/) library and a 
[Yara rules](https://yararules.com/). I decided to install both 
independently libraries from source, because I had some problems with 
[Guestfs](http://libguestfs.org/) internal integration of Yara. It is 
convenient to note that there is not a bug with 
[Yara feature](https://www.redhat.com/archives/libguestfs/2017-April/msg00084.html) 
in [Guestfs](http://libguestfs.org/). Versions that I used in my development enviroment are the following:

* **Libguestfs 1.40.2** - [http://download.libguestfs.org/](http://download.libguestfs.org/)
* **Libyara 3.11.0** - [https://github.com/VirusTotal/yara/releases](https://github.com/VirusTotal/yara/releases)

Compiling and installing from source have a serious maintenance problem but 
we can perform these steps in a finer-grained way. Furthermore repository 
packages are not up-to-date. Please, note that **Libguestfs** should be 
installed under ``/usr/local/lib``. For that purpose, execute ``make install``
as following:

```bash
sudo make INSTALLDIRS=vendor install
```

When everything is set up, please download *git* repository like the following:

```bash
$ git clone https://gitlab.com/luisfm/libguestfs-yara.git
$ cd libguestfs-yara
$ git checkout yara
```
It is convenient that there is a *QEMU* disk image that contains a malicious 
file under ``/etc/init.d/`` directory. You can check some ``defines`` 
directives in [yara_guestfs.c](./yara_guestfs.c) source code. In order to 
compile and run the *PoC*, please execute the following commands:

```bash
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
$ gcc yara_guestfs.c -lguestfs -o yara-guestfs
$ ./yara-guestfs
```

Code is a dirty *PoC*, so there are hardcoded paths. It is a bad praxis! 
Finally in the foreseeable future 
[Fitz Roy: a free solo climbing to sanitize virtual machines](https://gitlab.com/luisfm/fitz-roy) 
will have this feature.
