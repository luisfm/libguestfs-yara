
#include <stdio.h>
#include <stdlib.h>
#include <guestfs.h>

#define DRUPALGEDDON_RULE "./rules/Webshells/WShell_Drupalgeddon2_icos.yar"
#define VM_DISK "deb-infected.img"
#define ROOT_PARTITION "/dev/sda1"
#define GUEST_FILE "/etc/init.d/kns813.ico"

int main(char *argv[], int argc) 
{
    guestfs_h *g;
    char *vm_disk = VM_DISK; 
    int err;
    struct guestfs_yara_detection_list *detection_list;

    g = guestfs_create();
    if (g == NULL) {
        printf("Libguestfs could not be created\n");
        exit(EXIT_FAILURE);
    }

    guestfs_add_drive(g, vm_disk);
    err = guestfs_launch(g);
    if (err != 0) {
        printf("Libguestfs could not be launched\n");
        exit(EXIT_FAILURE);
    }

    err = guestfs_mount(g, ROOT_PARTITION, "/");
    if (err != 0) {
        printf("Libguestfs could not be mount %s partition\n", ROOT_PARTITION);
        exit(EXIT_FAILURE);
    }

    printf("[*] We are going to load %s precompiled rule\n", DRUPALGEDDON_RULE);
    err = guestfs_yara_load(g, DRUPALGEDDON_RULE);
    if (err != 0) {
        printf("Yara rule could not be load\n");
        exit(EXIT_FAILURE);
    }

    printf("[*] We are going to scan %s file\n", GUEST_FILE);
    /* for each matching rule a struct is returned, in this case, just one */
    detection_list = guestfs_yara_scan(g, GUEST_FILE);
    if (detection_list == NULL) {
        printf("An error has been launched when scan %s file\n", GUEST_FILE);
        exit(EXIT_FAILURE);
    }

    /* close guest filesystem and handler */
    guestfs_shutdown(g);
    guestfs_close(g);

    return 0;
}
